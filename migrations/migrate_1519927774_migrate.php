<?php

/**
 * Миграция 'migrate_1519926949_add_users'.
 */
class migrate_1519927774_migrate extends \marvin255\bxmigrate\migrate\Coded
{
    protected $users = [
        [
            "NAME"              => "Семён",
            "LAST_NAME"         => "Давыдов",
            "EMAIL"             => "test1@microsoft.com",
            "LOGIN"             => "test1",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Григорий",
            "LAST_NAME"         => "Громов",
            "EMAIL"             => "test2@microsoft.com",
            "LOGIN"             => "test2",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Матвей",
            "LAST_NAME"         => "Кулаков",
            "EMAIL"             => "test3@microsoft.com",
            "LOGIN"             => "test3",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Геннадий",
            "LAST_NAME"         => "Дорофеев",
            "EMAIL"             => "test4@microsoft.com",
            "LOGIN"             => "test4",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Денис",
            "LAST_NAME"         => "Герасимов",
            "EMAIL"             => "test5@microsoft.com",
            "LOGIN"             => "test5",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Екатерина",
            "LAST_NAME"         => "Большакова",
            "EMAIL"             => "test6@microsoft.com",
            "LOGIN"             => "test6",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Лариса",
            "LAST_NAME"         => "Афанасьева",
            "EMAIL"             => "test7@microsoft.com",
            "LOGIN"             => "test7",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
        [
            "NAME"              => "Римма",
            "LAST_NAME"         => "Орехова",
            "EMAIL"             => "test8@microsoft.com",
            "LOGIN"             => "test8",
            "ACTIVE"            => "Y",
            "PASSWORD"          => "123456",
        ],
    ];

    protected $departaments = [
        'нанодепартамент',
        '﻿госдепартамент',
        'департаментонлайна',
        'микродепартамент',
        'випдепартамент',
        'росдепартамент',
        'капиталдепартамент',
        'вечныйдепартамент',
    ];

    protected $photos = [
        'img/img1.jpg',
        'img/img2.jpg',
        'img/img3.jpg',
        'img/img4.jpg',
        'img/img5.jpg',
        'img/img6.jpg',
        'img/img7.jpg',
        'img/img8.jpg',
        'img/img9.jpg',
        'img/img10.jpg',
        'img/img11.jpg',
        'img/img12.jpg',
        'img/img13.jpg',
        'img/img14.jpg',
    ];

    protected $current_photo = 0;
    protected $photo_count = null;
    /**
     * @inheritdoc
     */
    public function up()
    {
        //set migration
        $user = new CUser;
        $IDs = [];
        foreach ($this->users as $user_array){
            $IDs[] = $user->Add($user_array);
        }

        $this->IblockTypeCreate([
            'ID' => 'test',
            'SECTIONS' => 'Y',
            'IN_RSS' => 'N',
            'SORT' => 500,
            'EDIT_FILE_BEFORE' => '',
            'EDIT_FILE_AFTER' => '',
            'LANG' => [
                'en' => [
                    'NAME' => 'Test',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ],
                'ru' => [
                    'NAME' => 'Тест',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ],
            ],
        ]);

        $this->IblockCreate(
            [
                'CODE' => 'departaments',
                'NAME' => 'Департаменты',
                'SORT' => 500,
                'IBLOCK_TYPE_ID' => 'test', //insert your iblock type id
                'VERSION' => 2,
                'INDEX_SECTION' => 'Y',
                'INDEX_ELEMENT' => 'Y',
                'LIST_MODE' => 'S',
                'LIST_PAGE_URL' => '',
                'SECTION_PAGE_URL' => '/#SECTION_CODE_PATH#/',
                'DETAIL_PAGE_URL' => '/#SECTION_CODE_PATH#/#CODE#/',
                'GROUP_ID' => [
                    1 => 'X',
                    2 => 'R',
                ],
            ],
            [
                'CODE' => [
                    'IS_REQUIRED' => 'N',
                    'DEFAULT_VALUE' => [
                        'TRANSLITERATION' => 'Y',
                        'TRANS_LEN' => 100,
                        'TRANS_CASE' => 'L',
                        'TRANS_SPACE' => '-',
                        'TRANS_OTHER' => '-',
                        'TRANS_EAT' => 'Y',
                    ],
                ],
                'SECTION_CODE' => [
                    'IS_REQUIRED' => 'N',
                    'DEFAULT_VALUE' => [
                        'TRANSLITERATION' => 'Y',
                        'TRANS_LEN' => 100,
                        'TRANS_CASE' => 'L',
                        'TRANS_SPACE' => '-',
                        'TRANS_OTHER' => '-',
                        'TRANS_EAT' => 'Y',
                    ],
                ],
            ]
        );

        $iblock = $this->IblockGetIdByCode('departaments');
        $this->IblockPropertyCreate(
            'departaments',
            [
                'NAME' => 'Начальник',
                'CODE' => 'chief',
                'SORT' => 500,
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'UserID',
                'IS_REQUIRED' => 'N', //Обязательное
                'MULTIPLE' => 'N',  //Множественное
                'WITH_DESCRIPTION' => 'N', //Выводить поле для описания
                'DEFAULT_VALUE' => '', //Значение по умолчанию
                'SEARCHABLE' => 'N', //Значения свойства участвуют в поиске
                'FILTRABLE' => 'N', //Поле для фильтрации
                'MULTIPLE_CNT' => 5, //Количество полей для ввода новых множественных значений
                'HINT' => '', //Подсказка
                'DISPLAY_EXPANDED' => 'N', //Показать развёрнутым
                'USER_TYPE_SETTINGS' => [], //Настройки поля
            ]
        );
        $this->IblockPropertyCreate(
            'departaments',
            [
                'NAME' => 'Фотографии',
                'CODE' => 'photos',
                'SORT' => 500,
                'PROPERTY_TYPE' => 'F',
                'IS_REQUIRED' => 'N', //Обязательное
                'MULTIPLE' => 'Y',  //Множественное
                'WITH_DESCRIPTION' => 'N', //Выводить поле для описания
                'DEFAULT_VALUE' => '', //Значение по умолчанию
                'SEARCHABLE' => 'N', //Значения свойства участвуют в поиске
                'FILTRABLE' => 'N', //Поле для фильтрации
                'MULTIPLE_CNT' => 5, //Количество полей для ввода новых множественных значений
                'HINT' => '', //Подсказка
                'DISPLAY_EXPANDED' => 'N', //Показать развёрнутым
                'USER_TYPE_SETTINGS' => [], //Настройки поля
            ]
        );

        foreach ($this->departaments as $k=>$departament){
            $el = new CIBlockElement;
            $el->Add([
                'NAME' => $departament,
                'IBLOCK_ID' => $iblock,
                'PROPERTY_VALUES' => [
                    'chief' => $IDs[$k],
                    'photos' => [
                        'n0' => $this->getPhoto(),
                        'n1' => $this->getPhoto(),
                        'n2' => $this->getPhoto(),
                        'n3' => $this->getPhoto(),
                        'n4' => $this->getPhoto(),
                    ]
                ]
            ]);
        }


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        //unset migration
    }

    protected function getPhoto(){
        if($this->photo_count === null){
            $this->photo_count = count($this->photos) - 1;
        }
        if($this->current_photo == $this->photo_count){
            $this->current_photo = 0;
        }
        $photo = \CFile::MakeFileArray(__DIR__ .'/'. $this->photos[$this->current_photo]);
        $this->current_photo++;
        return $photo;
    }
}

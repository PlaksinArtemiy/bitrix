<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отделы");

$APPLICATION->IncludeComponent(
	"test:department", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"NAME_COMPONENT" => "Список подразделений",
		"BLOCK_ID" => "3",
		"BLOCK_NAME" => "departaments",
		"HEIGHT_IMG" => "150",
		"WIDTH_IMG" => "150",
		"COUNT_SHOW_IMG" => "2",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/otdely/",
		"SEF_URL_TEMPLATES" => array(
			"list" => "index.php",
			"detail" => "#ELEMENT_ID#/",
		)
	),
	false
);

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
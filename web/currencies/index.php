<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Валюты");

$GLOBALS['filter'] = $APPLICATION->IncludeComponent(
	"creative.currenciesFilter", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "9600"
	),
	false
);

$APPLICATION->IncludeComponent(
	"creative.currencies", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"COUNT_ELEMENT_ON_PAGE" => "10",
		"COLUMNS_SHOW" => array(
			0 => "CODE_COURSE",
			1 => "DATE_COURSE",
			2 => "VALUE_COURSE",
		),
		"FILTER_NAME" => "filter"
	),
	false
);

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

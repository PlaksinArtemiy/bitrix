<?php

namespace creative\currencies;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config;

class CurrenciesTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'b_currencies';
    }

    public static function getMap()
    {

        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
            ),
            'CODE_COURSE' => array(
                'data_type' => 'string',
                'required' => true,
                'title' => Loc::getMessage('DATA_ENTITY_CODE_COURSE_FIELD'),
            ),
            'DATE_COURSE' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_DATE_COURSE_FIELD'),
            ),
            'VALUE_COURSE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('DATA_ENTITY_VALUE_COURSE_FIELD'),
            ),

        );
    }

    /**
     * Проверка наличия записи валюты по дате
     *
     * @param date $date
     *
     * @return boolean
     *
     */
    public static function GetCurrencyByDate($date)
    {
        // сделал так, что использование этой функции допускается только в том сдучае, если $date является датой
        $arFilter = [
            ">=DATE_COURSE" => $date . " 00:00:00",
            "<=DATE_COURSE" => $date . " 23:59:59"
        ];

        $res = CurrenciesTable::getList([
            'select' => [
                'CODE_COURSE'
            ],
            'filter' => $arFilter
        ]);

        return $res->fetchAll();
    }

    /**
     * Получение списка валют с сайта cbr.ru
     *
     * @return array
     *
     */
    public static function GetCurrenciesList()
    {
        $arResult = [];

        // запрос на сайт центрабанка для получения строки xml с курсами валют
        $option = new Config\Option();
        $url = $option::get("creative.currencies", "URLLoadCurrenciesGet", "fff");
        if ($url <> "") {

            $client = new \Bitrix\Main\Web\HttpClient();
            $xmlCurrencies = $client->get($url);

            require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');
            $objXML = new \CDataXML();
            $objXML->LoadString($xmlCurrencies);
            $arData = $objXML->GetArray();

            $cdate = \Bitrix\Main\Type\DateTime::createFromPhp(new \DateTime(ConvertDateTime($arData["ValCurs"]["@"]["Date"] . ' ' . date("H:i:s"), false, "ru")));

            for ($i = 0; $i < count($arData["ValCurs"]["#"]["Valute"]); $i++) {

                $CurrentCode = $arData["ValCurs"]["#"]["Valute"][$i]["#"]["CharCode"][0]["#"];
                $CurrentName = iconv('windows-1251', 'utf-8', $arData["ValCurs"]["#"]["Valute"][$i]["#"]["Name"][0]["#"]);
                $CurrentValue = str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$i]["#"]["Value"][0]["#"]);

                $arResult[] = ['CODE' => $CurrentCode, 'NAME' => $CurrentName, 'VALUE' => $CurrentValue, 'DATE' => $cdate];
            }

        }

        return $arResult;
    }

    /**
     * Проверка на наличие в двумерном массиве кода валюты
     *
     * $code string
     * $cerrencies array
     *
     * @return boolean
     *
     */
    public static function ExistenceCurrency($code, $currencies)
    {
        return array_filter($currencies, function ($var) use ($code) {
            return $var['CODE_COURSE'] === $code;
        });
    }

}


<?php

namespace creative\currencies;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use creative\currencies;
use Bitrix\Main\Config\Option;

class agent
{

    public static function AgentAutoLoadCurrencies()
    {
        // Получаем настройки модуля
        $AllowLoadCurrencies = Option::get("creative.currencies", "AllowLoadCurrencies", "N");

        if ($AllowLoadCurrencies == "Y") {

            $LoadListCurrencies = Option::get("creative.currencies", "LoadListCurrencies", []);

            $LoadListCurrencies = explode(",", $LoadListCurrencies);

            // Получаем все валюты с сайта центробоанка
            $res = currencies\CurrenciesTable::GetCurrenciesList();

            // Убираем из даты и времени время и оставляем только дату
            $loadDate = date("d.m.Y", MakeTimeStamp($res[0]['DATE'], "DD.MM.YYYY HH:MI:SS"));

            if (preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $loadDate))

            // Получаем весь массив валют из таблицы БД на дату
            $currenciesInDate = (preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $loadDate) ? currencies\CurrenciesTable::GetCurrencyByDate($loadDate) : []);

            // Идем циклом по всем валютом для вставки только нужных
            foreach ($res As $CurrentCurrencies) {

                // Проверяем, есть ли в массиве валют для загрузки валюты по которым идем циклом (сравниваем с параметрами в модуле)
                if (in_array($CurrentCurrencies['CODE'], $LoadListCurrencies)) {

                    // Проверяем, есть ли уже такая валюта за эту дату, если нету, тогда добавулям
                    if (!currencies\CurrenciesTable::ExistenceCurrency($CurrentCurrencies['CODE'], $currenciesInDate)) {

                        $result = currencies\CurrenciesTable::add(array(
                            'CODE_COURSE' => $CurrentCurrencies['CODE'],
                            'DATE_COURSE' => $CurrentCurrencies['DATE'],
                            'VALUE_COURSE' => $CurrentCurrencies['VALUE'],
                        ));

                        /*
                        Здесь можно сделать вывод какого-нибудь лога, в случае, если запись в таблицу БД не прошла
                        if ($result->isSuccess()) {
                            echo $result->getId();
                        } else {
                            echo print_r($result->getErrorMessages());
                        }
                        */
                    }

                }

            }
        }

        return "AgentAutoLoadCurrencies()";
    }

}

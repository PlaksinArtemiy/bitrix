<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CACHE_TIME' => array(
            'NAME' => 'Размер КЭШа',
            'TYPE' => 'STRING',
            'DEFAULT' => '3600',
        ),

        'COUNT_ELEMENT_ON_PAGE' => array(
            'NAME' => 'Элементов на странице',
            'TYPE' => 'INTEGER',
            'DEFAULT' => 10,
        ),

        'COLUMNS_SHOW' => array(
            'NAME' => 'Отображать колонки',
            'TYPE' => 'LIST',
            "MULTIPLE" => 'Y',
            'VALUES' => ['CODE_COURSE' => 'Код валюты', 'DATE_COURSE' => 'Дата', 'VALUE_COURSE' => 'Значение'],
        ),
    ),
);
?>
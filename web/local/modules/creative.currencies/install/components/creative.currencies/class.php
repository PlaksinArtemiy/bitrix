<?

use creative\currencies;
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CCurrenciesList extends CBitrixComponent
{

    public function executeComponent()
    {

        $this->arResult = [];
        if (Loader::includeModule("creative.currencies")) {

            $nav = new \Bitrix\Main\UI\PageNavigation("nav-course");
            $nav->allowAllRecords(true)
                ->setPageSize($this->arParams['COUNT_ELEMENT_ON_PAGE'])
                ->initFromUri();

            $arr = $this->arParams['COLUMNS_SHOW'];
            array_push($arr, 'ID');

            $query = [
                'select' => $arr,
                'order' => ["DATE_COURSE" => "DESC"],
                "count_total" => true,
                "offset" => $nav->getOffset(),
                "limit" => $nav->getLimit(),
            ];

            $arrFilter = $GLOBALS[$this->arParams['FILTER_NAME']]['FILTER'];
            if (!empty($arrFilter) and is_array($arrFilter)) {
                $query['filter'] = $arrFilter;
            }

            $res = currencies\CurrenciesTable::getList($query);

            $nav->setRecordCount($res->getCount());

            $this->arParams["NAVIGATION"] = $nav;

            $this->arParams["FIELDS"] = ['CODE_COURSE' => 'Код валюты', 'DATE_COURSE' => 'Дата', 'VALUE_COURSE' => 'Значение'];

            // Перебираем полученные записи
            while ($CurrentCurrencies = $res->fetch()) {
                foreach ($this->arParams['COLUMNS_SHOW'] As $keyField) {
                    $this->arResult[$CurrentCurrencies['ID']][$keyField] = $CurrentCurrencies[$keyField];
                }
            }

            $this->IncludeComponentTemplate();
        }
    }
}

;

?>
function checkDate(element, type) {
    if (element != "") {

        if (type == "date") {

            var elem = new Date(element);

            if (!elem.getTime()) return 0;
            else return 1;
        }
        else if (type == "number") {

            if(typeof element == type) return 1;
            else return 0;
        }
    }
    else return 1;
}

BX.ready(function(){
    applyFilterв.onclick = function() {

        var errorData = [];
        var messageFull = '';
        if (checkDate(dateStart.value, "date")==0) errorData.push('Не верно введена дата начала');
        if (checkDate(dateEnd.value, "date")==0) errorData.push('Не верно введена дата окончания');

        if (checkDate(valStart.value, "number")==0) errorData.push('Не верно введено число начала значения курса');
        if (checkDate(valEnd.value, "number")==0) errorData.push('Не верно введено число окончания значения курса');

        if (errorData.length>0) {
            errorData.forEach(function(mess) {
                messageFull = messageFull + '<br/>' + mess;
            });
            document.getElementById('errorMessage').innerHTML = messageFull;
            return false
        }
    };
});

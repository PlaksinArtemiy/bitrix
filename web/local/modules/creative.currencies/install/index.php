<?

use Bitrix\Main\Localization\Loc;
use creative\currencies;

Class creative_currencies extends CModule
{

    public function __construct()
    {

        if (file_exists(__DIR__ . "/version.php")) {

            $arModuleVersion = array();

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("MODULE_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("MODULE_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("MODULE_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("MODULE_PARTNER_URI");
        }

        return false;
    }

    function DoInstall()
    {

        global $DB, $APPLICATION, $step;

        $this->InstallDB();

        CopyDirFiles($this->getInstallatorPath() . '/components', $_SERVER["DOCUMENT_ROOT"] . "/local/components/", true, true);

        RegisterModule($this->MODULE_ID);

        var_dump(\Bitrix\Main\Loader::includeModule($this->MODULE_ID));

        CAgent::AddAgent(
            currencies\agent::AgentAutoLoadCurrencies(),
            $this->MODULE_ID,
            "N",
            86400,
            "02.04.2018 14:59:00",                // дата первой проверки на запуск
            "Y"                                      // агент активен
        );
        $APPLICATION->IncludeAdminFile(GetMessage('FORM_INSTALL_TITLE'), __DIR__ . "/step1.php");
    }

    function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;

        $DB->RunSQLBatch(__DIR__ . "/db/mysql/install.sql");

    }

    function DoUnInstall()
    {
        global $DB, $APPLICATION, $step;

        $this->unInstallDB();

        self::deleteByEtalon($this->getInstallatorPath() . '/components', $this->getComponentPath('components'));

        // CAgent::RemoveModuleAgents($this->MODULE_ID);

        UnRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(GetMessage('FORM_INSTALL_TITLE'), __DIR__ . "/unstep1.php");
    }

    /**
     * Проходится рекурсивно по содержимому папки назначения
     * и удаляет из него все пути эталонной папки
     *
     * @param string $etalon
     * @param string $dest
     *
     * @return null
     */
    protected static function deleteByEtalon($etalon, $dest)
    {
        $etalon = rtrim($etalon, '/\\');
        $dest = rtrim($dest, '/\\');
        if (!is_dir($etalon)) {
            throw new InvalidArgumentException("Path is not a directory: {$etalon}");
        } elseif (!is_dir($dest)) {
            throw new InvalidArgumentException("Path is not a directory: {$dest}");
        }
        foreach (scandir($etalon) as $file) {
            if ('.' === $file || '..' === $file || !file_exists($dest . '/' . $file)) continue;
            if (is_dir($dest . '/' . $file)) {
                self::deleteByEtalon($etalon . '/' . $file, $dest . '/' . $file);
            } else {
                unlink($dest . '/' . $file);
            }
        }
        $content = array_diff(scandir($dest), ['..', '.']);
        if (!$content) rmdir($dest);
    }

    /**
     * Возвращает путь к папке с модулем
     *
     * @return string
     */
    public function getInstallatorPath()
    {
        return str_replace('\\', '/', __DIR__);
    }

    /**
     * Возвращает путь к папке, в которую будут установлены компоненты модуля.
     *
     * @param string $type тип компонентов для установки (components, js, admin и т.д.)
     *
     * @return string
     */
    public function getComponentPath($type = 'components')
    {
        if ($type === 'admin') {
            $base = Application::getDocumentRoot() . '/bitrix';
        } else {
            $base = dirname(dirname(dirname($this->getInstallatorPath())));
        }

        return $base . '/' . str_replace(['/', '.'], '', $type);
    }


    function unInstallDB()
    {
        global $DB, $DBType, $APPLICATION;

        $DB->RunSQLBatch(__DIR__ . "/db/mysql/uninstall.sql");

    }


}

?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use creative\currencies;

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use    Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;


Loc::loadMessages(__FILE__);

//$request = HttpApplication::getInstance()->getContext()->getRequest();
$request = Application::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

// Собираем в массив все возможные валюты с сайта cbr.ru
$res = currencies\CurrenciesTable::GetCurrenciesList();
foreach ($res As $CurrentCurrencies) {
    $arrCurrencies[$CurrentCurrencies['CODE']] = $CurrentCurrencies['NAME'] . ' (' . $CurrentCurrencies['CODE'] . ')';
}

$aTabs = array(
    array(
        "DIV" => "edit",
        "TAB" => "Настройки модуля",
        "TITLE" => "Настройки модуля",
        "OPTIONS" => array(
            "Настройки загрузки валют",
            array(
                "AllowLoadCurrencies",
                "Выполнять загрузку:",
                "Y",
                array("checkbox")
            ),
            array(
                "URLLoadCurrenciesGet",
                "Адрес загрузки валют:",
                "http://www.cbr.ru/scripts/XML_daily.asp",
                array("text", 43)
            ),
            array(
                "LoadListCurrencies",
                "Загружать валюты",
                "normal",
                array("multiselectbox", $arrCurrencies)
            )
        )
    )
);


///////////////// Обработчик формы. Начало //////////////////

if ($request->isPost() && check_bitrix_sessid()) {

    foreach ($aTabs as $aTab) {

        foreach ($aTab["OPTIONS"] as $arOption) {

            if (!is_array($arOption)) {

                continue;
            }

            if ($arOption["note"]) {

                continue;
            }

            if ($request["apply"]) {

                $optionValue = $request->getPost($arOption[0]);

                if ($arOption[0] == "AllowLoadCurrencies") {

                    if ($optionValue == "") {

                        $optionValue = "N";
                    }
                }

                Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
            } elseif ($request["default"]) {

                Option::set($module_id, $arOption[0], $arOption[2]);
            }
        }
    }

}

///////////////// Обработчик формы. Конец //////////////////

$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs
);

$tabControl->Begin();

?>

    <form action="<? echo($APPLICATION->GetCurPage()); ?>?lang=<?= $request->getQuery("lang"); ?>&mid=<? echo($module_id); ?>&mid_menu=<?= $request->getQuery("mid_menu"); ?>"
          method="post">

        <?
        foreach ($aTabs as $aTab) {

            if ($aTab["OPTIONS"]) {

                $tabControl->BeginNextTab();

                __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
            }
        }

        $tabControl->Buttons();
        ?>

        <input type="submit" name="apply" value="Применить" class="adm-btn-save"/>
        <input type="submit" name="default" value="По умолчанию"/>

        <?
        echo(bitrix_sessid_post());
        ?>

    </form>


<?

$tabControl->End();

?>
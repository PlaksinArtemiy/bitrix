<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (is_array($arResult)): ?>

    <div class="divTable">
        <div class="divTableBody">
            <div class="divTableRow">

                <?php foreach ($arParams['COLUMNS_SHOW'] As $keyField): ?>
                    <div class="divTableCell"><?= $arParams['FIELDS'][$keyField] ?></div>
                <?php endforeach; ?>
            </div>


            <?php foreach ($arResult As $currentCourse): ?>
                <div class="divTableRow">
                    <?php foreach ($arParams['COLUMNS_SHOW'] As $keyField): ?>
                        <div class="divTableCell"><?= $currentCourse[$keyField]; ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.pagenavigation",
                "",
                array(
                    "NAV_OBJECT" => $arParams['NAVIGATION'],
                    "SEF_MODE" => "N",
                ),
                false
            );
            ?>
        </div>
    </div>
<? endif; ?>




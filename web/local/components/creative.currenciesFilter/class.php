<?

use creative\currencies;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CCurrenciesFilter extends CBitrixComponent
{

    public function setFilter($filter, $selected_courses, $val_start, $val_end, $date_start, $date_end)
    {
        $_SESSION['BX']['FILTER'] = $filter;
        $_SESSION['BX']['SELECTED_COURSES'] = $selected_courses;
        $_SESSION['BX']['FILTER_VALUE']['VAL_START'] = $val_start;
        $_SESSION['BX']['FILTER_VALUE']['VAL_END'] = $val_end;

        $_SESSION['BX']['FILTER_VALUE']['DATE_START'] = $date_start;
        $_SESSION['BX']['FILTER_VALUE']['DATE_END'] = $date_end;
    }

    public function executeComponent()
    {
        $this->arResult = [];
        if (Loader::includeModule("creative.currencies")) {

            if (isset($_POST["applyFilter"])) {
                $selectCurrencies = $_POST['selectCurrencies'];

                $arFilter = [];
                if (is_array($selectCurrencies)) {
                    $arFilter['=CODE_COURSE'] = $selectCurrencies;
                }

                $valStart = trim($_POST['valStart']);
                $valEnd = trim($_POST['valEnd']);
                $dateStart = trim($_POST['dateStart']);
                $dateEnd = trim($_POST['dateEnd']);


                $error = [];
                if (trim($_POST['dateStart'])) {
                    if (preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateStart'])) {
                        $arFilter['>=DATE_COURSE'] = $dateStart . ' 00:00:00';
                    } else {
                        $error[] = 'Не верный формат даты начала';
                    }
                }

                if (trim($_POST['dateEnd'])) {
                    if (preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateEnd'])) {
                        echo $dateEnd;
                        $arFilter['<=DATE_COURSE'] = $dateEnd . ' 23:59:59';
                    } else {
                        $error[] = 'Не верный формат даты окончания';
                    }
                }

                if (trim($_POST['valStart'])) {
                    if (is_numeric(trim($_POST['valStart']))) {
                        $arFilter['>=VALUE_COURSE'] = $valStart;
                    } else {
                        $error[] = 'Не верно введено число начала значения курса';
                    }
                }

                if (trim($_POST['valEnd'])) {
                    if (is_numeric(trim($_POST['valEnd']))) {
                        $arFilter['<=VALUE_COURSE'] = $valEnd;
                    } else {
                        $error[] = 'Не верно введено число окончания значения курса';
                    }
                }

                $this->setFilter($arFilter, $selectCurrencies, $valStart, $valEnd, $dateStart, $dateEnd);

            }

            if (isset($_POST["cancelFilter"])) {
                $this->setFilter([], [], '', '', '', '');
            }

            $cache = Bitrix\Main\Data\Cache::createInstance();
            if ($cache->initCache($this->arParams['CACHE_TIME'], "currenciesFilterList")) {
                $res = $cache->getVars();
            } elseif ($cache->startDataCache()) {
                // Получаем только уникальные значения из таблицы БД
                $res = currencies\CurrenciesTable::getList([
                    'select' => [
                        'CODE_COURSE'
                    ],
                    'group' => [
                        'CODE_COURSE'
                    ]
                ]);
                $res = (array)$res->fetchAll();
                $cache->endDataCache($res);
            }

            // Перебираем полученные записи
            foreach ($res as $key => $CurrentCurrencies) {
                $this->arResult['CODE'][$CurrentCurrencies['CODE_COURSE']]['CODE'] = $CurrentCurrencies['CODE_COURSE'];
                if (in_array($CurrentCurrencies['CODE_COURSE'], $_SESSION['BX']['SELECTED_COURSES'])) {
                    $this->arResult['CODE'][$CurrentCurrencies['CODE_COURSE']]['STATUS'] = 'selected';
                }
            }

            $this->arResult['VAL_START'] = $_SESSION['BX']['FILTER_VALUE']['VAL_START'];
            $this->arResult['VAL_END'] = $_SESSION['BX']['FILTER_VALUE']['VAL_END'];

            $this->arResult['DATE_START'] = $_SESSION['BX']['FILTER_VALUE']['DATE_START'];
            $this->arResult['DATE_END'] = $_SESSION['BX']['FILTER_VALUE']['DATE_END'];

            $this->arResult['FILTER'] = $_SESSION['BX']['FILTER'];

            $this->arResult['ERROR_FILTER'] = $error;

        }

        $this->IncludeComponentTemplate();

        return $this->arResult;

    }
}

?>
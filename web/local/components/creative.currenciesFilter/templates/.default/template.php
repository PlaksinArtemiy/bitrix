<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (is_array($arResult)): ?>

    <div class="filter-currencies">
        <div id="errorMessage">
            <?php foreach ($arResult['ERROR_FILTER'] as $error): ?>
                <div class="divTableCell"><?= $error; ?></div>
            <?php endforeach; ?>
        </div>

        <form method="post" action="<?= POST_FORM_ACTION_URI; ?>">
            <div class="left-filter-block left-filter-block-width">
                <select class="select-filter" name="selectCurrencies[]" size="7" multiple>
                    <?php foreach ($arResult['CODE'] as $codeByFilter): ?>
                        <div class="divTableCell"><?= $error; ?></div>
                        <option <?= $codeByFilter['STATUS']; ?>
                                value="<?= $codeByFilter['CODE']; ?>"><?= $codeByFilter['CODE']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="left-filter-block">
                Период: с: <input class="input-data" id="dateStart" name="dateStart" type="text"
                                  value="<?= $arResult['DATE_START']; ?>"> по: <input class="input-data" name="dateEnd"
                                                                                      id="dateEnd" type="text"
                                                                                      value="<?= $arResult['DATE_END']; ?>"><br>
                Значение курса: с: <input class="input-data" id="valStart" name="valStart" type="text"
                                          value="<?= $arResult['VAL_START']; ?>"> по: <input class="input-data"
                                                                                             id="valEnd" name="valEnd"
                                                                                             type="text"
                                                                                             value="<?= $arResult['VAL_END']; ?>"><br>

            </div>
            <div class="clear"></div>

            <input type="submit" id="applyFilter" name="applyFilter" value="Применить фильтр">
            <input type="submit" name="cancelFilter" value="Сбросить фильтр">
        </form>
    </div>
<?php endif; ?>
